package equipo8.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMvAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoMvAppApplication.class, args);
	}

}
